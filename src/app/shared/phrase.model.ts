export class Phrase{
    constructor(public eng: string, public ptBr:string){
        this.eng = eng;
        this.ptBr = ptBr;
    }
}