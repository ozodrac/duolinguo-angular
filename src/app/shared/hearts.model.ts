export class Hearts{
    constructor(public filled: boolean, public urlFilledHeart: string = "/assets/coracao_cheio.png", public urlEmptyHeart: string = "/assets/coracao_vazio.png"){

    }

    public showHeart(){
        if(this.filled){
            return this.urlFilledHeart;
        }else{
            return this.urlEmptyHeart;
        }
    }
}