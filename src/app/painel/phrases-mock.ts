import {Phrase} from '../shared/phrase.model';

export const Frases : Phrase[] = [
    {eng:'I like to Learn', ptBr:'Eu gosto de Aprender'},
    {eng:'Shazam Modafoca!', ptBr:'Shazam Carai'},
    {eng:'there inside!', ptBr:'Ai dento!'},
    {eng:'I play soccer!', ptBr:'eu jogo futebol!'},
]