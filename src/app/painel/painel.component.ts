import { Component, OnInit, EventEmitter, Output, OnDestroy } from '@angular/core';
import { Phrase } from '../shared/phrase.model';
import { Frases } from './phrases-mock';

@Component({
  selector: 'app-painel',
  templateUrl: './painel.component.html',
  styleUrls: ['./painel.component.css']
})
export class PainelComponent implements OnInit ,OnDestroy{

  public instruction : string = " Translate the Phrase below: ";

  public frase : Phrase[] = Frases;
  public resposta : string = "";

  public rodada : number = 0;
  public rodadaFrase: Phrase;

  public progress: number = 0;

  public tentativas: number = 3;

  @Output() public encerrarJogo: EventEmitter<Object> = new EventEmitter();

  constructor() {
    this.atualizaRodada()
    console.log(this.rodadaFrase);
  }

  ngOnInit() {
  }

  ngOnDestroy(){
    console.log('destroyed')
  }

  atualizaRodada():void{
    this.rodadaFrase = this.frase[this.rodada];
    this.resposta = "";
    console.log(this.rodadaFrase);
  }
  atualizaResposta(resposta: Event):void{
    this.resposta = (<HTMLInputElement>resposta.target).value
  }
  verificarResposta():void{

    if(this.resposta == this.rodadaFrase.ptBr){
      this.progress +=  (100/this.frase.length);
      this.rodada++;
      if(this.rodada === this.frase.length){
        console.log(this.encerrarJogo)
        this.encerrarJogo.emit({message:'Victory Beloved Foca',color:'color:green;'})
      }
      this.atualizaRodada()
    }else{
      this.tentativas--;
      if(this.tentativas <=-1){
        this.encerrarJogo.emit({message:'Tv Perdestes, ó Grande pederasta fornicador de capivaras imundas!',color:'color:red;'})
      }
    }
  }

}
