import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'duo';

  public jogoEmAndamento:boolean =true;
  public mensagemEncerramento:object;

  public encerrarJogo(tipo:Object):void{
    console.log(tipo)
    this.jogoEmAndamento=false;
    this.mensagemEncerramento=tipo
  }
  public rebootGame(){
    this.jogoEmAndamento=true;
    this.mensagemEncerramento = undefined;
  }
}
