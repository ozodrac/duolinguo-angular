import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { Hearts } from '../shared/hearts.model';

@Component({
  selector: 'app-tentativas',
  templateUrl: './tentativas.component.html',
  styleUrls: ['./tentativas.component.css']
})
export class TentativasComponent implements OnInit,OnChanges {

  constructor() { }

  ngOnChanges(){
    if ((this.tentativas !== this.hearts.length)){
      let index = this.hearts.length - this.tentativas;
      if(index <= this.hearts.length){
        this.hearts[index-1].filled = false;
      }
    }
  }
  ngOnInit() {
  }

  @Input() public tentativas:number=3; //recebe um dado do componente pai para o componente filho

  public hearts : Hearts[] = [
    new Hearts(true),
    new Hearts(true),
    new Hearts(true),
  ]
}
